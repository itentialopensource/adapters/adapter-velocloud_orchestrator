/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-velocloud_orchestrator',
      type: 'VelocloudOrchestrator',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const VelocloudOrchestrator = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] VelocloudOrchestrator Adapter Test', () => {
  describe('VelocloudOrchestrator Class Tests', () => {
    const a = new VelocloudOrchestrator(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
          && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const allConfigurationCloneEnterpriseTemplateBodyParam = {
      enterpriseId: 9,
      configurationType: 'NETWORK_BASED',
      name: 'string',
      description: 'string'
    };
    describe('#configurationCloneEnterpriseTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationCloneEnterpriseTemplate(allConfigurationCloneEnterpriseTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'configurationCloneEnterpriseTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allConfigurationDeleteConfigurationBodyParam = {
      id: 6
    };
    describe('#configurationDeleteConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationDeleteConfiguration(allConfigurationDeleteConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'configurationDeleteConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allConfigurationGetConfigurationBodyParam = {
      id: 10
    };
    describe('#configurationGetConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationGetConfiguration(allConfigurationGetConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SEGMENT_BASED', data.response.configurationType);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.edgeCount);
                assert.equal('string', data.response.effective);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.logicalId);
                assert.equal('string', data.response.modified);
                assert.equal(true, Array.isArray(data.response.modules));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.schemaVersion);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'configurationGetConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allConfigurationGetIdentifiableApplicationsBodyParam = {
      enterpriseId: 8
    };
    describe('#configurationGetIdentifiableApplications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationGetIdentifiableApplications(allConfigurationGetIdentifiableApplicationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.applicationClassToApplication);
                assert.equal('object', typeof data.response.applicationToApplicationClass);
                assert.equal(true, Array.isArray(data.response.applications));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'configurationGetIdentifiableApplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allConfigurationGetRoutableApplicationsBodyParam = {
      enterpriseId: 5,
      edgeId: 6
    };
    describe('#configurationGetRoutableApplications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationGetRoutableApplications(allConfigurationGetRoutableApplicationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.applicationClassToApplication);
                assert.equal('object', typeof data.response.applicationToApplicationClass);
                assert.equal(true, Array.isArray(data.response.applications));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'configurationGetRoutableApplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allDisasterRecoveryConfigureActiveForReplicationBodyParam = {
      standbyList: [
        {}
      ],
      drVCOUser: 'string',
      drVCOPassword: 'string'
    };
    describe('#disasterRecoveryConfigureActiveForReplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disasterRecoveryConfigureActiveForReplication(allDisasterRecoveryConfigureActiveForReplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'disasterRecoveryConfigureActiveForReplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allDisasterRecoveryDemoteActiveBodyParam = {
      force: true
    };
    describe('#disasterRecoveryDemoteActive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disasterRecoveryDemoteActive(allDisasterRecoveryDemoteActiveBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'disasterRecoveryDemoteActive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allDisasterRecoveryGetReplicationBlobBodyParam = {};
    describe('#disasterRecoveryGetReplicationBlob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disasterRecoveryGetReplicationBlob(allDisasterRecoveryGetReplicationBlobBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.activeAccessFromStandby);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'disasterRecoveryGetReplicationBlob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allDisasterRecoveryGetReplicationStatusBodyParam = {
      with: [
        'storageInfo'
      ]
    };
    describe('#disasterRecoveryGetReplicationStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disasterRecoveryGetReplicationStatus(allDisasterRecoveryGetReplicationStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.activeAddress);
                assert.equal('string', data.response.activeReplicationAddress);
                assert.equal(true, Array.isArray(data.response.clientContact));
                assert.equal('object', typeof data.response.clientCount);
                assert.equal('STANDBY_BACKGROUND_IMPORT', data.response.drState);
                assert.equal('string', data.response.drVCOUser);
                assert.equal('string', data.response.failureDescription);
                assert.equal('string', data.response.lastDrProtectedTime);
                assert.equal('ZOMBIE', data.response.role);
                assert.equal('string', data.response.roleTimestamp);
                assert.equal(true, Array.isArray(data.response.standbyList));
                assert.equal(true, Array.isArray(data.response.stateHistory));
                assert.equal('string', data.response.stateTimestamp);
                assert.equal('string', data.response.vcoIp);
                assert.equal('string', data.response.vcoReplicationIp);
                assert.equal('string', data.response.vcoUuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'disasterRecoveryGetReplicationStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allDisasterRecoveryPrepareForStandbyBodyParam = {};
    describe('#disasterRecoveryPrepareForStandby - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disasterRecoveryPrepareForStandby(allDisasterRecoveryPrepareForStandbyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'disasterRecoveryPrepareForStandby', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allDisasterRecoveryPromoteStandbyToActiveBodyParam = {
      force: true
    };
    describe('#disasterRecoveryPromoteStandbyToActive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disasterRecoveryPromoteStandbyToActive(allDisasterRecoveryPromoteStandbyToActiveBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'disasterRecoveryPromoteStandbyToActive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allDisasterRecoveryRemoveStandbyBodyParam = {};
    describe('#disasterRecoveryRemoveStandby - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disasterRecoveryRemoveStandby(allDisasterRecoveryRemoveStandbyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'disasterRecoveryRemoveStandby', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allDisasterRecoveryTransitionToStandbyBodyParam = {
      activeAccessFromStandby: 'string'
    };
    describe('#disasterRecoveryTransitionToStandby - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disasterRecoveryTransitionToStandby(allDisasterRecoveryTransitionToStandbyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'disasterRecoveryTransitionToStandby', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEdgeDeleteEdgeBodyParam = {
      enterpriseId: 3,
      id: 6,
      ids: [
        7
      ]
    };
    describe('#edgeDeleteEdge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeDeleteEdge(allEdgeDeleteEdgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'edgeDeleteEdge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEdgeDeleteEdgeBgpNeighborRecordsBodyParam = {
      records: [
        {
          edgeId: 4,
          neighborIp: 'string'
        }
      ]
    };
    describe('#edgeDeleteEdgeBgpNeighborRecords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeDeleteEdgeBgpNeighborRecords(allEdgeDeleteEdgeBgpNeighborRecordsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'edgeDeleteEdgeBgpNeighborRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEdgeSetEdgeEnterpriseConfigurationBodyParam = {
      configurationId: 5
    };
    describe('#edgeSetEdgeEnterpriseConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeSetEdgeEnterpriseConfiguration(allEdgeSetEdgeEnterpriseConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'edgeSetEdgeEnterpriseConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEdgeSetEdgeHandOffGatewaysBodyParam = {
      edgeId: 2,
      enterpriseId: 2,
      gateways: {
        primary: 1,
        primaryIpsecDetail: {
          ipsecGatewayAddress: 'string',
          strictHostCheck: false,
          strictHostCheckDN: 'string'
        },
        secondary: 8,
        secondaryIpsecDetail: {
          ipsecGatewayAddress: 'string',
          strictHostCheck: false,
          strictHostCheckDN: 'string'
        }
      }
    };
    describe('#edgeSetEdgeHandOffGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeSetEdgeHandOffGateways(allEdgeSetEdgeHandOffGatewaysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'edgeSetEdgeHandOffGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEdgeSetEdgeOperatorConfigurationBodyParam = {
      edgeId: 4,
      configurationId: 9
    };
    describe('#edgeSetEdgeOperatorConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeSetEdgeOperatorConfiguration(allEdgeSetEdgeOperatorConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'edgeSetEdgeOperatorConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseDeleteEnterpriseBodyParam = {
      enterpriseId: 5
    };
    describe('#enterpriseDeleteEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseDeleteEnterprise(allEnterpriseDeleteEnterpriseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseDeleteEnterprise', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseDeleteEnterpriseGatewayRecordsBodyParam = {
      records: [
        {
          gatewayId: 8,
          neighborIp: 'string'
        }
      ]
    };
    describe('#enterpriseDeleteEnterpriseGatewayRecords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseDeleteEnterpriseGatewayRecords(allEnterpriseDeleteEnterpriseGatewayRecordsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseDeleteEnterpriseGatewayRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseDeleteEnterpriseNetworkAllocationBodyParam = {
      id: 7
    };
    describe('#enterpriseDeleteEnterpriseNetworkAllocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseDeleteEnterpriseNetworkAllocation(allEnterpriseDeleteEnterpriseNetworkAllocationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseDeleteEnterpriseNetworkAllocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseDeleteEnterpriseNetworkSegmentBodyParam = {
      id: 1
    };
    describe('#enterpriseDeleteEnterpriseNetworkSegment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseDeleteEnterpriseNetworkSegment(allEnterpriseDeleteEnterpriseNetworkSegmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.error);
                assert.equal(8, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseDeleteEnterpriseNetworkSegment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseDeleteEnterpriseServiceBodyParam = {
      id: 6,
      logicalId: 'string',
      enterpriseId: 10
    };
    describe('#enterpriseDeleteEnterpriseService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseDeleteEnterpriseService(allEnterpriseDeleteEnterpriseServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseDeleteEnterpriseService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseAddressesBodyParam = {
      enterpriseId: 4
    };
    describe('#enterpriseGetEnterpriseAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseAddresses(allEnterpriseGetEnterpriseAddressesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseAlertConfigurationsBodyParam = {
      enterpriseId: 1
    };
    describe('#enterpriseGetEnterpriseAlertConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseAlertConfigurations(allEnterpriseGetEnterpriseAlertConfigurationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseAlertConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseAlertsBodyParam = {
      enterpriseId: 1,
      interval: {}
    };
    describe('#enterpriseGetEnterpriseAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseAlerts(allEnterpriseGetEnterpriseAlertsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metaData);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseAllAlertRecipientsBodyParam = {
      enterpriseId: 10
    };
    describe('#enterpriseGetEnterpriseAllAlertRecipients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseAllAlertRecipients(allEnterpriseGetEnterpriseAllAlertRecipientsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.emailEnabled);
                assert.equal(true, Array.isArray(data.response.emailList));
                assert.equal(true, Array.isArray(data.response.enterpriseUsers));
                assert.equal(false, data.response.mobileEnabled);
                assert.equal(true, Array.isArray(data.response.mobileList));
                assert.equal(false, data.response.smsEnabled);
                assert.equal(true, Array.isArray(data.response.smsList));
                assert.equal(true, data.response.snmpEnabled);
                assert.equal(true, Array.isArray(data.response.snmpList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseAllAlertRecipients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseConfigurationsBodyParam = {
      enterpriseId: 8
    };
    describe('#enterpriseGetEnterpriseConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseConfigurations(allEnterpriseGetEnterpriseConfigurationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseNetworkAllocationsBodyParam = {
      enterpriseId: 2,
      name: 'string',
      with: [
        'edgeCount'
      ]
    };
    describe('#enterpriseGetEnterpriseNetworkAllocations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseNetworkAllocations(allEnterpriseGetEnterpriseNetworkAllocationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseNetworkAllocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseNetworkSegmentsBodyParam = {
      enterpriseId: 10,
      name: 'string',
      type: 'string',
      with: [
        'profileCount'
      ]
    };
    describe('#enterpriseGetEnterpriseNetworkSegments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseNetworkSegments(allEnterpriseGetEnterpriseNetworkSegmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseNetworkSegments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseRouteConfigurationBodyParam = {
      enterpriseId: 4
    };
    describe('#enterpriseGetEnterpriseRouteConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseRouteConfiguration(allEnterpriseGetEnterpriseRouteConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(9, data.response.operatorId);
                assert.equal(10, data.response.networkId);
                assert.equal(10, data.response.enterpriseId);
                assert.equal(2, data.response.edgeId);
                assert.equal(10, data.response.gatewayId);
                assert.equal(6, data.response.parentGroupId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.object);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.logicalId);
                assert.equal(0, data.response.alertsEnabled);
                assert.equal(0, data.response.operatorAlertsEnabled);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusModified);
                assert.equal('object', typeof data.response.previousData);
                assert.equal('string', data.response.previousCreated);
                assert.equal('string', data.response.draftData);
                assert.equal('string', data.response.draftCreated);
                assert.equal('string', data.response.draftComment);
                assert.equal('object', typeof data.response.data);
                assert.equal('string', data.response.lastContact);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseRouteConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseRouteTableBodyParam = {
      enterpriseId: 10,
      profiles: [
        {
          id: 10,
          name: 'string',
          description: 'string'
        }
      ],
      subnets: [
        {
          subnet: 'string',
          preferredExits: [
            {
              type: 'string',
              exitType: 'string',
              edgeId: 9,
              edgeName: 'string',
              profileId: 4,
              cidrIp: 'string',
              cost: 9,
              advertise: true
            }
          ],
          eligableExits: [
            {
              type: 'string',
              exitType: 'string',
              edgeId: 7,
              edgeName: 'string',
              profileId: 1,
              cidrIp: 'string',
              cost: 2,
              advertise: true
            }
          ]
        }
      ]
    };
    describe('#enterpriseGetEnterpriseRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseRouteTable(allEnterpriseGetEnterpriseRouteTableBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.profiles));
                assert.equal(true, Array.isArray(data.response.subnets));
                assert.equal('object', typeof data.response.exits);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseGetEnterpriseServicesBodyParam = {
      enterpriseId: 7,
      type: 'string',
      with: [
        'configuration'
      ],
      name: 'string',
      includePartnerGateways: false
    };
    describe('#enterpriseGetEnterpriseServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseServices(allEnterpriseGetEnterpriseServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseGetEnterpriseServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseInsertEnterpriseServiceBodyParam = {
      type: 'string',
      name: 'string',
      data: {}
    };
    describe('#enterpriseInsertEnterpriseService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertEnterpriseService(allEnterpriseInsertEnterpriseServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseInsertEnterpriseService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseInsertOrUpdateEnterpriseAlertConfigurationsBodyParam = {
      enterpriseAlertConfigurations: [
        {}
      ]
    };
    describe('#enterpriseInsertOrUpdateEnterpriseAlertConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertOrUpdateEnterpriseAlertConfigurations(allEnterpriseInsertOrUpdateEnterpriseAlertConfigurationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.enterpriseAlertConfigurations));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseInsertOrUpdateEnterpriseAlertConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseSetEnterpriseAllAlertRecipientsBodyParam = {
      enterpriseId: 10,
      enterpriseUsers: [
        {
          enterpriseUserId: 6,
          enabled: false,
          smsEnabled: false,
          emailEnabled: false,
          mobileEnabled: false
        }
      ],
      smsEnabled: false,
      smsList: [
        {
          username: 'string',
          mobilePhone: 'string',
          email: 'string'
        }
      ],
      emailEnabled: false,
      emailList: [
        'string'
      ],
      mobileEnabled: true,
      mobileList: [
        'string'
      ]
    };
    describe('#enterpriseSetEnterpriseAllAlertRecipients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseSetEnterpriseAllAlertRecipients(allEnterpriseSetEnterpriseAllAlertRecipientsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.emailEnabled);
                assert.equal(true, Array.isArray(data.response.emailList));
                assert.equal(true, Array.isArray(data.response.enterpriseUsers));
                assert.equal(false, data.response.mobileEnabled);
                assert.equal(true, Array.isArray(data.response.mobileList));
                assert.equal(false, data.response.smsEnabled);
                assert.equal(true, Array.isArray(data.response.smsList));
                assert.equal(false, data.response.snmpEnabled);
                assert.equal(true, Array.isArray(data.response.snmpList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseSetEnterpriseAllAlertRecipients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseSetEnterpriseOperatorConfigurationBodyParam = {
      enterpriseId: 7,
      configurationId: 4
    };
    describe('#enterpriseSetEnterpriseOperatorConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseSetEnterpriseOperatorConfiguration(allEnterpriseSetEnterpriseOperatorConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseSetEnterpriseOperatorConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseUpdateEnterpriseRouteBodyParam = {
      enterpriseId: 8,
      original: {
        eligableExits: [
          {
            type: 'string',
            exitType: 'string',
            edgeId: 5,
            edgeName: 'string',
            profileId: 1,
            cidrIp: 'string',
            cost: 2,
            advertise: true
          }
        ],
        preferredExits: [
          {
            type: 'string',
            exitType: 'string',
            edgeId: 5,
            edgeName: 'string',
            profileId: 4,
            cidrIp: 'string',
            cost: 6,
            advertise: false
          }
        ],
        subnet: 'string'
      },
      updated: {
        eligableExits: [
          {
            type: 'string',
            exitType: 'string',
            edgeId: 4,
            edgeName: 'string',
            profileId: 3,
            cidrIp: 'string',
            cost: 10,
            advertise: true
          }
        ],
        preferredExits: [
          {
            type: 'string',
            exitType: 'string',
            edgeId: 1,
            edgeName: 'string',
            profileId: 4,
            cidrIp: 'string',
            cost: 3,
            advertise: false
          }
        ],
        subnet: 'string'
      }
    };
    describe('#enterpriseUpdateEnterpriseRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUpdateEnterpriseRoute(allEnterpriseUpdateEnterpriseRouteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.eligableExits));
                assert.equal(true, Array.isArray(data.response.preferredExits));
                assert.equal('string', data.response.subnet);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseUpdateEnterpriseRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseUpdateEnterpriseRouteConfigurationBodyParam = {
      data: {}
    };
    describe('#enterpriseUpdateEnterpriseRouteConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUpdateEnterpriseRouteConfiguration(allEnterpriseUpdateEnterpriseRouteConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseUpdateEnterpriseRouteConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseUpdateEnterpriseServiceBodyParam = {
      id: 2,
      _update: {}
    };
    describe('#enterpriseUpdateEnterpriseService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUpdateEnterpriseService(allEnterpriseUpdateEnterpriseServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseUpdateEnterpriseService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseProxyDeleteEnterpriseProxyUserBodyParam = {
      enterpriseProxyId: 5,
      id: 9,
      username: 'string'
    };
    describe('#enterpriseProxyDeleteEnterpriseProxyUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyDeleteEnterpriseProxyUser(allEnterpriseProxyDeleteEnterpriseProxyUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseProxyDeleteEnterpriseProxyUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseProxyGetEnterpriseProxyEdgeInventoryBodyParam = {
      enterpriseProxyId: 10
    };
    describe('#enterpriseProxyGetEnterpriseProxyEdgeInventory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyGetEnterpriseProxyEdgeInventory(allEnterpriseProxyGetEnterpriseProxyEdgeInventoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseProxyGetEnterpriseProxyEdgeInventory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseProxyGetEnterpriseProxyEnterprisesBodyParam = {
      enterpriseProxyId: 6,
      with: [
        'edges'
      ]
    };
    describe('#enterpriseProxyGetEnterpriseProxyEnterprises - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyGetEnterpriseProxyEnterprises(allEnterpriseProxyGetEnterpriseProxyEnterprisesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseProxyGetEnterpriseProxyEnterprises', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseProxyGetEnterpriseProxyGatewayPoolsBodyParam = {
      id: 8,
      enterpriseProxyId: 7,
      with: [
        'gateways'
      ]
    };
    describe('#enterpriseProxyGetEnterpriseProxyGatewayPools - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyGetEnterpriseProxyGatewayPools(allEnterpriseProxyGetEnterpriseProxyGatewayPoolsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseProxyGetEnterpriseProxyGatewayPools', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseProxyGetEnterpriseProxyOperatorProfilesBodyParam = {
      enterpriseProxyId: 3,
      with: [
        'edges'
      ]
    };
    describe('#enterpriseProxyGetEnterpriseProxyOperatorProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyGetEnterpriseProxyOperatorProfiles(allEnterpriseProxyGetEnterpriseProxyOperatorProfilesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseProxyGetEnterpriseProxyOperatorProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseProxyGetEnterpriseProxyUserBodyParam = {
      id: 8,
      username: 'string'
    };
    describe('#enterpriseProxyGetEnterpriseProxyUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyGetEnterpriseProxyUser(allEnterpriseProxyGetEnterpriseProxyUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.userType);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.officePhone);
                assert.equal('string', data.response.mobilePhone);
                assert.equal(true, data.response.isNative);
                assert.equal(true, data.response.isActive);
                assert.equal(true, data.response.isLocked);
                assert.equal(true, data.response.disableSecondFactor);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.modified);
                assert.equal(2, data.response.roleId);
                assert.equal('string', data.response.roleName);
                assert.equal(5, data.response.enterpriseProxyId);
                assert.equal(1, data.response.networkId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseProxyGetEnterpriseProxyUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseProxyInsertEnterpriseProxyUserBodyParam = {
      id: 2,
      created: 'string',
      userType: 'string',
      username: 'string',
      domain: 'string',
      password: 'string',
      firstName: 'string',
      lastName: 'string',
      officePhone: 'string',
      mobilePhone: 'string',
      isNative: true,
      isActive: true,
      isLocked: true,
      disableSecondFactor: true,
      email: 'string',
      lastLogin: 'string',
      modified: 'string',
      enterpriseId: 1,
      enterpriseProxyId: 4,
      roleId: 3
    };
    describe('#enterpriseProxyInsertEnterpriseProxyUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyInsertEnterpriseProxyUser(allEnterpriseProxyInsertEnterpriseProxyUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseProxyInsertEnterpriseProxyUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseProxyUpdateEnterpriseProxyUserBodyParam = {
      _update: {}
    };
    describe('#enterpriseProxyUpdateEnterpriseProxyUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyUpdateEnterpriseProxyUser(allEnterpriseProxyUpdateEnterpriseProxyUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseProxyUpdateEnterpriseProxyUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseUserDeleteEnterpriseUserBodyParam = {
      id: 5,
      enterpriseId: 8,
      username: 'string'
    };
    describe('#enterpriseUserDeleteEnterpriseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUserDeleteEnterpriseUser(allEnterpriseUserDeleteEnterpriseUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseUserDeleteEnterpriseUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseUserGetEnterpriseUserBodyParam = {
      id: 2,
      username: 'string'
    };
    describe('#enterpriseUserGetEnterpriseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUserGetEnterpriseUser(allEnterpriseUserGetEnterpriseUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.userType);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.officePhone);
                assert.equal('string', data.response.mobilePhone);
                assert.equal(true, data.response.isNative);
                assert.equal(true, data.response.isActive);
                assert.equal(true, data.response.isLocked);
                assert.equal(false, data.response.disableSecondFactor);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.modified);
                assert.equal(10, data.response.roleId);
                assert.equal('string', data.response.roleName);
                assert.equal(4, data.response.enterpriseId);
                assert.equal(9, data.response.enterpriseProxyId);
                assert.equal(6, data.response.networkId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseUserGetEnterpriseUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allEnterpriseUserUpdateEnterpriseUserBodyParam = {
      _update: {}
    };
    describe('#enterpriseUserUpdateEnterpriseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUserUpdateEnterpriseUser(allEnterpriseUserUpdateEnterpriseUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'enterpriseUserUpdateEnterpriseUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allLoginEnterpriseLoginBodyParam = {
      password: 'string',
      username: 'string'
    };
    describe('#loginEnterpriseLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.loginEnterpriseLogin(allLoginEnterpriseLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'loginEnterpriseLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allLoginOperatorLoginBodyParam = {
      password: 'string',
      username: 'string'
    };
    describe('#loginOperatorLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.loginOperatorLogin(allLoginOperatorLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'loginOperatorLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLogout((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'postLogout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allApiPath = 'fakedata';
    describe('#postMetaApiPath - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetaApiPath(allApiPath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.privileges);
                assert.equal('object', typeof data.response.swagger);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'postMetaApiPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeAppLinkMetricsBodyParam = {
      id: 3,
      interval: {}
    };
    describe('#metricsGetEdgeAppLinkMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeAppLinkMetrics(allMetricsGetEdgeAppLinkMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeAppLinkMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeAppLinkSeriesBodyParam = {
      id: 10,
      interval: {}
    };
    describe('#metricsGetEdgeAppLinkSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeAppLinkSeries(allMetricsGetEdgeAppLinkSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeAppLinkSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeAppMetricsBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeAppMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeAppMetrics(allMetricsGetEdgeAppMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeAppMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeAppSeriesBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeAppSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeAppSeries(allMetricsGetEdgeAppSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeAppSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeCategoryMetricsBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeCategoryMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeCategoryMetrics(allMetricsGetEdgeCategoryMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeCategoryMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeCategorySeriesBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeCategorySeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeCategorySeries(allMetricsGetEdgeCategorySeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeCategorySeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeDestMetricsBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeDestMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeDestMetrics(allMetricsGetEdgeDestMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeDestMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeDestSeriesBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeDestSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeDestSeries(allMetricsGetEdgeDestSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeDestSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeDeviceMetricsBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeDeviceMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeDeviceMetrics(allMetricsGetEdgeDeviceMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeDeviceMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeDeviceSeriesBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeDeviceSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeDeviceSeries(allMetricsGetEdgeDeviceSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeDeviceSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeLinkMetricsBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeLinkMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeLinkMetrics(allMetricsGetEdgeLinkMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response[0]));
                assert.equal(true, Array.isArray(data.response[1]));
                assert.equal(true, Array.isArray(data.response[2]));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeLinkMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeLinkSeriesBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeLinkSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeLinkSeries(allMetricsGetEdgeLinkSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeLinkSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeOsMetricsBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeOsMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeOsMetrics(allMetricsGetEdgeOsMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeOsMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeOsSeriesBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeOsSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeOsSeries(allMetricsGetEdgeOsSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeOsSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeSegmentMetricsBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeSegmentMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeSegmentMetrics(allMetricsGetEdgeSegmentMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeSegmentMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeSegmentSeriesBodyParam = {
      interval: {}
    };
    describe('#metricsGetEdgeSegmentSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeSegmentSeries(allMetricsGetEdgeSegmentSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeSegmentSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeStatusMetricsBodyParam = {
      edgeId: 4
    };
    describe('#metricsGetEdgeStatusMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeStatusMetrics(allMetricsGetEdgeStatusMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal('object', typeof data.response.tunnelCount);
                assert.equal('object', typeof data.response.memoryPct);
                assert.equal('object', typeof data.response.flowCount);
                assert.equal('object', typeof data.response.cpuPct);
                assert.equal('object', typeof data.response.handoffQueueDrops);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeStatusMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetEdgeStatusSeriesBodyParam = {
      edgeId: 4
    };
    describe('#metricsGetEdgeStatusSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetEdgeStatusSeries(allMetricsGetEdgeStatusSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.series));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetEdgeStatusSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetGatewayStatusMetricsBodyParam = {
      gatewayId: 9
    };
    describe('#metricsGetGatewayStatusMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetGatewayStatusMetrics(allMetricsGetGatewayStatusMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('object', typeof data.response.tunnelCount);
                assert.equal('object', typeof data.response.memoryPct);
                assert.equal('object', typeof data.response.flowCount);
                assert.equal('object', typeof data.response.cpuPct);
                assert.equal('object', typeof data.response.handoffQueueDrops);
                assert.equal('object', typeof data.response.connectedEdges);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetGatewayStatusMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMetricsGetGatewayStatusSeriesBodyParam = {
      gatewayId: 10
    };
    describe('#metricsGetGatewayStatusSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.metricsGetGatewayStatusSeries(allMetricsGetGatewayStatusSeriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.series));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'metricsGetGatewayStatusSeries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMonitoringGetAggregateEdgeLinkMetricsBodyParam = {
      enterprises: [
        1
      ],
      metrics: [
        'p3BytesRx'
      ],
      interval: {
        end: 'string',
        start: 'string'
      }
    };
    describe('#monitoringGetAggregateEdgeLinkMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.monitoringGetAggregateEdgeLinkMetrics(allMonitoringGetAggregateEdgeLinkMetricsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'monitoringGetAggregateEdgeLinkMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMonitoringGetAggregateEnterpriseEventsBodyParam = {
      detail: false,
      interval: {
        end: 'string',
        start: 'string'
      },
      filter: {
        limit: 2048
      }
    };
    describe('#monitoringGetAggregateEnterpriseEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.monitoringGetAggregateEnterpriseEvents(allMonitoringGetAggregateEnterpriseEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'monitoringGetAggregateEnterpriseEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMonitoringGetAggregatesBodyParam = {
      enterpriseId: 7,
      id: 9
    };
    describe('#monitoringGetAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.monitoringGetAggregates(allMonitoringGetAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.edgeCount);
                assert.equal('object', typeof data.response.edges);
                assert.equal(true, Array.isArray(data.response.enterprises));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'monitoringGetAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMonitoringGetEnterpriseBgpPeerStatusBodyParam = {
      enterpriseId: 10
    };
    describe('#monitoringGetEnterpriseBgpPeerStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.monitoringGetEnterpriseBgpPeerStatus(allMonitoringGetEnterpriseBgpPeerStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'monitoringGetEnterpriseBgpPeerStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMonitoringGetEnterpriseEdgeBgpPeerStatusBodyParam = {
      enterpriseId: 8
    };
    describe('#monitoringGetEnterpriseEdgeBgpPeerStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.monitoringGetEnterpriseEdgeBgpPeerStatus(allMonitoringGetEnterpriseEdgeBgpPeerStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'monitoringGetEnterpriseEdgeBgpPeerStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMonitoringGetEnterpriseEdgeClusterStatusBodyParam = {
      enterpriseId: 3
    };
    describe('#monitoringGetEnterpriseEdgeClusterStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.monitoringGetEnterpriseEdgeClusterStatus(allMonitoringGetEnterpriseEdgeClusterStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'monitoringGetEnterpriseEdgeClusterStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allMonitoringGetEnterpriseEdgeLinkStatusBodyParam = {
      enterprises: [
        1
      ],
      enterpriseProxyId: 9,
      networkId: 4,
      links: true
    };
    describe('#monitoringGetEnterpriseEdgeLinkStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.monitoringGetEnterpriseEdgeLinkStatus(allMonitoringGetEnterpriseEdgeLinkStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'monitoringGetEnterpriseEdgeLinkStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allNetworkGetNetworkConfigurationsBodyParam = {
      id: 3,
      networkId: 3,
      with: [
        'counts'
      ]
    };
    describe('#networkGetNetworkConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkGetNetworkConfigurations(allNetworkGetNetworkConfigurationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'networkGetNetworkConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allNetworkGetNetworkEnterprisesBodyParam = {
      networkId: 9,
      id: 9,
      with: [
        'edges'
      ]
    };
    describe('#networkGetNetworkEnterprises - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkGetNetworkEnterprises(allNetworkGetNetworkEnterprisesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'networkGetNetworkEnterprises', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allNetworkGetNetworkGatewayPoolsBodyParam = {
      id: 10,
      networkId: 5,
      with: [
        'enterprises'
      ]
    };
    describe('#networkGetNetworkGatewayPools - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkGetNetworkGatewayPools(allNetworkGetNetworkGatewayPoolsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'networkGetNetworkGatewayPools', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allNetworkGetNetworkOperatorUsersBodyParam = {
      networkId: 10
    };
    describe('#networkGetNetworkOperatorUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkGetNetworkOperatorUsers(allNetworkGetNetworkOperatorUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'networkGetNetworkOperatorUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allOperatorUserDeleteOperatorUserBodyParam = {
      id: 4,
      username: 'string'
    };
    describe('#operatorUserDeleteOperatorUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.operatorUserDeleteOperatorUser(allOperatorUserDeleteOperatorUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'operatorUserDeleteOperatorUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allOperatorUserGetOperatorUserBodyParam = {
      id: 10,
      username: 'string'
    };
    describe('#operatorUserGetOperatorUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.operatorUserGetOperatorUser(allOperatorUserGetOperatorUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(9, data.response.operatorId);
                assert.equal('string', data.response.userType);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.officePhone);
                assert.equal('string', data.response.mobilePhone);
                assert.equal(true, data.response.isNative);
                assert.equal(false, data.response.isActive);
                assert.equal(false, data.response.isLocked);
                assert.equal(true, data.response.disableSecondFactor);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.modified);
                assert.equal(2, data.response.roleId);
                assert.equal('string', data.response.roleName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'operatorUserGetOperatorUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allOperatorUserInsertOperatorUserBodyParam = {
      id: 4,
      created: 'string',
      userType: 'string',
      username: 'string',
      domain: 'string',
      password: 'string',
      firstName: 'string',
      lastName: 'string',
      officePhone: 'string',
      mobilePhone: 'string',
      isNative: true,
      isActive: false,
      isLocked: false,
      disableSecondFactor: true,
      email: 'string',
      lastLogin: 'string',
      modified: 'string',
      networkId: 3,
      roleId: 3
    };
    describe('#operatorUserInsertOperatorUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.operatorUserInsertOperatorUser(allOperatorUserInsertOperatorUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(9, data.response.operatorId);
                assert.equal('string', data.response.userType);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.officePhone);
                assert.equal('string', data.response.mobilePhone);
                assert.equal(true, data.response.isNative);
                assert.equal(true, data.response.isActive);
                assert.equal(false, data.response.isLocked);
                assert.equal(false, data.response.disableSecondFactor);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.modified);
                assert.equal(10, data.response.roleId);
                assert.equal('string', data.response.roleName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'operatorUserInsertOperatorUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allOperatorUserUpdateOperatorUserBodyParam = {
      _update: {}
    };
    describe('#operatorUserUpdateOperatorUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.operatorUserUpdateOperatorUser(allOperatorUserUpdateOperatorUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'operatorUserUpdateOperatorUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allRoleCreateRoleCustomizationBodyParam = {
      forRoleId: 2,
      privilegeIds: [
        6
      ]
    };
    describe('#roleCreateRoleCustomization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.roleCreateRoleCustomization(allRoleCreateRoleCustomizationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'roleCreateRoleCustomization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allRoleDeleteRoleCustomizationBodyParam = {
      forRoleId: 9
    };
    describe('#roleDeleteRoleCustomization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.roleDeleteRoleCustomization(allRoleDeleteRoleCustomizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'roleDeleteRoleCustomization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allRoleGetUserTypeRolesBodyParam = {
      userType: 'OPERATOR'
    };
    describe('#roleGetUserTypeRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.roleGetUserTypeRoles(allRoleGetUserTypeRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'roleGetUserTypeRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allVcoDiagnosticsGetVcoDbDiagnosticsBodyParam = {};
    describe('#vcoDiagnosticsGetVcoDbDiagnostics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vcoDiagnosticsGetVcoDbDiagnostics(allVcoDiagnosticsGetVcoDbDiagnosticsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'vcoDiagnosticsGetVcoDbDiagnostics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allVcoInventoryAssociateEdgeBodyParam = {
      edgeInventoryId: 5,
      enterpriseId: 2,
      edgeId: 4,
      deviceSerialNumber: 'string'
    };
    describe('#vcoInventoryAssociateEdge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vcoInventoryAssociateEdge(allVcoInventoryAssociateEdgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.deviceSerialNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'vcoInventoryAssociateEdge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allVcoInventoryGetInventoryItemsBodyParam = {
      enterpriseId: 10,
      modifiedSince: 7,
      with: [
        'edge'
      ]
    };
    describe('#vcoInventoryGetInventoryItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vcoInventoryGetInventoryItems(allVcoInventoryGetInventoryItemsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'vcoInventoryGetInventoryItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const allVpnGenerateVpnGatewayConfigurationBodyParam = {
      name: 'string',
      data: {}
    };
    describe('#vpnGenerateVpnGatewayConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vpnGenerateVpnGatewayConfiguration(allVpnGenerateVpnGatewayConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('All', 'vpnGenerateVpnGatewayConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const clientDeviceSetClientDeviceHostNameBodyParam = {
      enterpriseId: 2,
      hostName: 'string'
    };
    describe('#setClientDeviceHostName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setClientDeviceHostName(clientDeviceSetClientDeviceHostNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ClientDevice', 'setClientDeviceHostName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationConfigurationCloneAndConvertConfigurationBodyParam = {
      configurationId: 7
    };
    describe('#configurationCloneAndConvertConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationCloneAndConvertConfiguration(configurationConfigurationCloneAndConvertConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'configurationCloneAndConvertConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationConfigurationCloneConfigurationBodyParam = {
      configurationId: 9
    };
    describe('#configurationCloneConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationCloneConfiguration(configurationConfigurationCloneConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'configurationCloneConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationConfigurationGetConfigurationModulesBodyParam = {
      configurationId: 2
    };
    describe('#configurationGetConfigurationModules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationGetConfigurationModules(configurationConfigurationGetConfigurationModulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'configurationGetConfigurationModules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationConfigurationInsertConfigurationModuleBodyParam = {
      name: 'properties',
      data: {},
      configurationId: 5
    };
    describe('#configurationInsertConfigurationModule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationInsertConfigurationModule(configurationConfigurationInsertConfigurationModuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'configurationInsertConfigurationModule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationConfigurationUpdateConfigurationBodyParam = {
      id: 7,
      _update: {}
    };
    describe('#configurationUpdateConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationUpdateConfiguration(configurationConfigurationUpdateConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.error);
                assert.equal(4, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'configurationUpdateConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationConfigurationUpdateConfigurationModuleBodyParam = {
      id: 3,
      configurationModuleId: 8,
      enterpriseId: 6,
      basic: true,
      _update: {
        created: 'string',
        effective: 'string',
        id: 5,
        isSmallData: 1,
        modified: 'string',
        name: 'firewall',
        type: 'ENTERPRISE',
        description: 'string',
        configurationId: 3,
        data: {},
        refs: {},
        schemaVersion: 'string'
      }
    };
    describe('#configurationUpdateConfigurationModule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configurationUpdateConfigurationModule(configurationConfigurationUpdateConfigurationModuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'configurationUpdateConfigurationModule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeEdgeCancelReactivationBodyParam = {
      enterpriseId: 3,
      edgeId: 5
    };
    describe('#edgeEdgeCancelReactivation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeEdgeCancelReactivation(edgeEdgeEdgeCancelReactivationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeEdgeCancelReactivation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeEdgeProvisionBodyParam = {
      configurationId: 9,
      modelNumber: 'virtual'
    };
    describe('#edgeEdgeProvision - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeEdgeProvision(edgeEdgeEdgeProvisionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.activationKey);
                assert.equal('object', typeof data.response.generatedCertificate);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeEdgeProvision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeEdgeRequestReactivationBodyParam = {
      enterpriseId: 4,
      edgeId: 6
    };
    describe('#edgeEdgeRequestReactivation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeEdgeRequestReactivation(edgeEdgeEdgeRequestReactivationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.activationKey);
                assert.equal('string', data.response.activationKeyExpires);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeEdgeRequestReactivation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeGetClientVisibilityModeBodyParam = {
      edgeId: 1
    };
    describe('#edgeGetClientVisibilityMode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeGetClientVisibilityMode(edgeEdgeGetClientVisibilityModeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('MAC', data.response.edgeClientVisibilityMode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeGetClientVisibilityMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeGetEdgeBodyParam = {
      id: 7,
      enterpriseId: 3,
      logicalId: 'string',
      activationKey: 'string',
      name: 'string',
      with: [
        'links'
      ]
    };
    describe('#edgeGetEdge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeGetEdge(edgeEdgeGetEdgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.activationKey);
                assert.equal('string', data.response.activationKeyExpires);
                assert.equal('UNASSIGNED', data.response.activationState);
                assert.equal('string', data.response.activationTime);
                assert.equal(1, data.response.alertsEnabled);
                assert.equal('string', data.response.buildNumber);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.deviceFamily);
                assert.equal('string', data.response.deviceId);
                assert.equal('string', data.response.dnsName);
                assert.equal('EXPIRED', data.response.edgeState);
                assert.equal('string', data.response.edgeStateTime);
                assert.equal('CERTIFICATE_OPTIONAL', data.response.endpointPkiMode);
                assert.equal(9, data.response.enterpriseId);
                assert.equal('string', data.response.haLastContact);
                assert.equal('UNCONFIGURED', data.response.haPreviousState);
                assert.equal('string', data.response.haSerialNumber);
                assert.equal('PENDING_DISSOCIATION', data.response.haState);
                assert.equal(1, data.response.id);
                assert.equal(5, data.response.isLive);
                assert.equal('string', data.response.lastContact);
                assert.equal('string', data.response.logicalId);
                assert.equal('string', data.response.modelNumber);
                assert.equal('string', data.response.modified);
                assert.equal('string', data.response.name);
                assert.equal(0, data.response.operatorAlertsEnabled);
                assert.equal('string', data.response.selfMacAddress);
                assert.equal('string', data.response.serialNumber);
                assert.equal('IN_SERVICE', data.response.serviceState);
                assert.equal('string', data.response.serviceUpSince);
                assert.equal(8, data.response.siteId);
                assert.equal('string', data.response.softwareUpdated);
                assert.equal('string', data.response.softwareVersion);
                assert.equal('string', data.response.systemUpSince);
                assert.equal('object', typeof data.response.configuration);
                assert.equal('object', typeof data.response.enterprise);
                assert.equal(true, Array.isArray(data.response.links));
                assert.equal(true, Array.isArray(data.response.recentLinks));
                assert.equal('object', typeof data.response.site);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeGetEdge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeGetEdgeActivationEmailBodyParam = {
      edgeId: 1,
      enterpriseId: 1,
      ipVersion: [
        'IPv6',
        'IPv4'
      ]
    };
    describe('#edgeGetEdgeActivationEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeGetEdgeActivationEmail(edgeGetEdgeActivationEmailBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.to);
                assert.equal(null, data.response.cc);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.replyTo);
                assert.equal('string', data.response.subject);
                assert.equal('object', typeof data.response.content);
                assert.equal('string', data.response.content.activationURL);
                assert.equal('string', data.response.content.activationURLIpV6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeGetEdgeActivationEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeGetEdgeConfigurationModulesBodyParam = {
      edgeId: 7
    };
    describe('#edgeGetEdgeConfigurationModules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeGetEdgeConfigurationModules(edgeEdgeGetEdgeConfigurationModulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.deviceSettings);
                assert.equal('object', typeof data.response.firewall);
                assert.equal('object', typeof data.response.QOS);
                assert.equal('object', typeof data.response.WAN);
                assert.equal('object', typeof data.response.controlPlane);
                assert.equal('object', typeof data.response.managementPlane);
                assert.equal('object', typeof data.response.imageUpdate);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeGetEdgeConfigurationModules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeGetEdgeConfigurationStackBodyParam = {
      edgeId: 5
    };
    describe('#edgeGetEdgeConfigurationStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeGetEdgeConfigurationStack(edgeEdgeGetEdgeConfigurationStackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeGetEdgeConfigurationStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeSendEdgeActivationEmailBodyParam = {
      enterpriseId: 1,
      name: 'string',
      edgeId: 1,
      to: 'string',
      from: 'string',
      replyTo: 'string',
      subject: 'string',
      bcc: '',
      cc: '',
      content: {
        salutation: 'string',
        prompt: 'string',
        message: 'string',
        activationURL: 'string',
        steps: [
          'string',
          'string',
          'string'
        ]
      }
    };
    describe('#edgeSendEdgeActivationEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeSendEdgeActivationEmail(edgeSendEdgeActivationEmailBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.accepted);
                assert.equal('object', typeof data.response.rejected);
                assert.equal(1, data.response.envelopeTime);
                assert.equal(1, data.response.messageTime);
                assert.equal('string', data.response.response);
                assert.equal('object', typeof data.response.envelope);
                assert.equal('string', data.response.envelope.from);
                assert.equal('string', data.response.envelope.to);
                assert.equal('string', data.response.messageId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeSendEdgeActivationEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeUpdateEdgeAdminPasswordBodyParam = {
      id: 6,
      password: 'string'
    };
    describe('#edgeUpdateEdgeAdminPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeUpdateEdgeAdminPassword(edgeEdgeUpdateEdgeAdminPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeUpdateEdgeAdminPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeUpdateEdgeAttributesBodyParam = {
      _update: {}
    };
    describe('#edgeUpdateEdgeAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeUpdateEdgeAttributes(edgeEdgeUpdateEdgeAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeUpdateEdgeAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const edgeEdgeUpdateEdgeCredentialsByConfigurationBodyParam = {
      credentials: {}
    };
    describe('#edgeUpdateEdgeCredentialsByConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.edgeUpdateEdgeCredentialsByConfiguration(edgeEdgeUpdateEdgeCredentialsByConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.actionIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Edge', 'edgeUpdateEdgeCredentialsByConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseDecodeEnterpriseKeyBodyParam = {
      key: 'string'
    };
    describe('#enterpriseDecodeEnterpriseKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseDecodeEnterpriseKey(enterpriseEnterpriseDecodeEnterpriseKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseDecodeEnterpriseKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseEncodeEnterpriseKeyBodyParam = {
      key: 'string'
    };
    describe('#enterpriseEncodeEnterpriseKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseEncodeEnterpriseKey(enterpriseEnterpriseEncodeEnterpriseKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseEncodeEnterpriseKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseGetEnterpriseBodyParam = {
      id: 2,
      enterpriseId: 5,
      with: [
        'enterpriseProxy'
      ]
    };
    describe('#enterpriseGetEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterprise(enterpriseEnterpriseGetEnterpriseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(5, data.response.networkId);
                assert.equal(7, data.response.gatewayPoolId);
                assert.equal(1, data.response.alertsEnabled);
                assert.equal(0, data.response.operatorAlertsEnabled);
                assert.equal('CERTIFICATE_OPTIONAL', data.response.endpointPkiMode);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.prefix);
                assert.equal('string', data.response.logicalId);
                assert.equal('string', data.response.accountNumber);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.contactName);
                assert.equal('string', data.response.contactPhone);
                assert.equal('string', data.response.contactMobile);
                assert.equal('string', data.response.contactEmail);
                assert.equal('string', data.response.streetAddress);
                assert.equal('string', data.response.streetAddress2);
                assert.equal('string', data.response.city);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.postalCode);
                assert.equal('string', data.response.country);
                assert.equal(6, data.response.lat);
                assert.equal(2, data.response.lon);
                assert.equal('string', data.response.timezone);
                assert.equal('string', data.response.locale);
                assert.equal('string', data.response.modified);
                assert.equal('object', typeof data.response.enterpriseProxy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseGetEnterprise', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseGetEnterpriseCapabilitiesBodyParam = {
      enterpriseId: 2
    };
    describe('#enterpriseGetEnterpriseCapabilities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseCapabilities(enterpriseEnterpriseGetEnterpriseCapabilitiesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response['edgeVnfs.enable']);
                assert.equal(true, data.response['edgeVnfs.securityVnf.paloAlto']);
                assert.equal(false, data.response['edgeVnfs.securityVnf.checkpoint']);
                assert.equal(false, data.response['edgeVnfs.securityVnf.fortinet']);
                assert.equal(false, data.response.enableBGP);
                assert.equal(true, data.response.enableCosMapping);
                assert.equal(false, data.response.enableEnterpriseAuth);
                assert.equal(false, data.response.enableFwLogs);
                assert.equal(true, data.response.enableStatefulFirewall);
                assert.equal(true, data.response.enableNetworks);
                assert.equal(false, data.response.enableOSPF);
                assert.equal(false, data.response.enablePKI);
                assert.equal(true, data.response.enablePremium);
                assert.equal(false, data.response.enableSegmentation);
                assert.equal(true, data.response.enableServiceRateLimiting);
                assert.equal(true, data.response.enableVQM);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseGetEnterpriseCapabilities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseGetEnterpriseEdgesBodyParam = {
      id: 8,
      enterpriseId: 4,
      with: [
        'certificates'
      ]
    };
    describe('#enterpriseGetEnterpriseEdges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseEdges(enterpriseEnterpriseGetEnterpriseEdgesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseGetEnterpriseEdges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseGetEnterpriseGatewayHandoffBodyParam = {
      enterpriseId: 1
    };
    describe('#enterpriseGetEnterpriseGatewayHandoff - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enterpriseGetEnterpriseGatewayHandoff(enterpriseEnterpriseGetEnterpriseGatewayHandoffBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseGetEnterpriseGatewayHandoff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseGetEnterpriseMaximumSegmentsBodyParam = {
      enterpriseId: 5
    };
    describe('#enterpriseGetEnterpriseMaximumSegments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseMaximumSegments(enterpriseEnterpriseGetEnterpriseMaximumSegmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal(10, data.response.enterpriseId);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.isPassword);
                assert.equal('DATE', data.response.dataType);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseGetEnterpriseMaximumSegments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseGetEnterpriseNetworkAllocationBodyParam = {
      id: 8
    };
    describe('#enterpriseGetEnterpriseNetworkAllocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseNetworkAllocation(enterpriseEnterpriseGetEnterpriseNetworkAllocationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(5, data.response.operatorId);
                assert.equal(10, data.response.networkId);
                assert.equal(8, data.response.enterpriseId);
                assert.equal(8, data.response.edgeId);
                assert.equal(3, data.response.gatewayId);
                assert.equal(1, data.response.parentGroupId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.object);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.logicalId);
                assert.equal(1, data.response.alertsEnabled);
                assert.equal(1, data.response.operatorAlertsEnabled);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusModified);
                assert.equal('object', typeof data.response.previousData);
                assert.equal('string', data.response.previousCreated);
                assert.equal('string', data.response.draftData);
                assert.equal('string', data.response.draftCreated);
                assert.equal('string', data.response.draftComment);
                assert.equal('object', typeof data.response.data);
                assert.equal('string', data.response.lastContact);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseGetEnterpriseNetworkAllocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseGetEnterprisePropertyBodyParam = {
      enterpriseId: 4,
      name: 'string',
      id: 3
    };
    describe('#enterpriseGetEnterpriseProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseProperty(enterpriseEnterpriseGetEnterprisePropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal(10, data.response.enterpriseId);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.value);
                assert.equal(true, data.response.isPassword);
                assert.equal('STRING', data.response.dataType);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseGetEnterpriseProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseProxyGetEnterpriseProxyPropertyBodyParam = {
      enterpriseId: 8,
      name: 'string',
      id: 5
    };
    describe('#enterpriseProxyGetEnterpriseProxyProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyGetEnterpriseProxyProperty(enterpriseEnterpriseProxyGetEnterpriseProxyPropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal(5, data.response.enterpriseProxyId);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.value);
                assert.equal(true, data.response.isPassword);
                assert.equal('JSON', data.response.dataType);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseProxyGetEnterpriseProxyProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseInsertEnterpriseBodyParam = {
      id: 8,
      created: 'string',
      name: 'string',
      contactName: 'string',
      contactPhone: 'string',
      contactMobile: 'string',
      contactEmail: 'string',
      streetAddress: 'string',
      streetAddress2: 'string',
      city: 'string',
      state: 'string',
      postalCode: 'string',
      country: 'string',
      lat: 9,
      lon: 3,
      timezone: 'string',
      locale: 'string',
      shippingSameAsLocation: 0,
      shippingContactName: 'string',
      shippingAddress: 'string',
      shippingAddress2: 'string',
      shippingCity: 'string',
      shippingState: 'string',
      shippingCountry: 'string',
      shippingPostalCode: 'string',
      modified: 'string',
      gatewayPoolId: 4,
      networkId: 4,
      returnData: false,
      user: {
        email: 'string',
        password: 'string',
        password2: 'string',
        username: 'string'
      },
      configurationId: 10,
      enableEnterpriseDelegationToOperator: false,
      enableEnterpriseDelegationToProxy: false,
      enableEnterpriseUserManagementDelegationToOperator: false,
      licenses: {
        ids: [
          6
        ]
      }
    };
    describe('#enterpriseInsertEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertEnterprise(enterpriseEnterpriseInsertEnterpriseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseInsertEnterprise', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseInsertEnterpriseNetworkAllocationBodyParam = {
      name: 'string'
    };
    describe('#enterpriseInsertEnterpriseNetworkAllocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertEnterpriseNetworkAllocation(enterpriseEnterpriseInsertEnterpriseNetworkAllocationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseInsertEnterpriseNetworkAllocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseInsertEnterpriseNetworkSegmentBodyParam = {
      name: 'string',
      type: 'CDE',
      data: {}
    };
    describe('#enterpriseInsertEnterpriseNetworkSegment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertEnterpriseNetworkSegment(enterpriseEnterpriseInsertEnterpriseNetworkSegmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseInsertEnterpriseNetworkSegment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseInsertOrUpdateEnterpriseCapabilityBodyParam = {
      name: 'enableCosMapping',
      value: true
    };
    describe('#enterpriseInsertOrUpdateEnterpriseCapability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertOrUpdateEnterpriseCapability(enterpriseEnterpriseInsertOrUpdateEnterpriseCapabilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseInsertOrUpdateEnterpriseCapability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseInsertOrUpdateEnterpriseGatewayHandoffBodyParam = {};
    describe('#enterpriseInsertOrUpdateEnterpriseGatewayHandoff - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertOrUpdateEnterpriseGatewayHandoff(enterpriseEnterpriseInsertOrUpdateEnterpriseGatewayHandoffBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseInsertOrUpdateEnterpriseGatewayHandoff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseInsertOrUpdateEnterprisePropertyBodyParam = {
      enterpriseId: 10,
      name: 'string',
      value: 'string'
    };
    describe('#enterpriseInsertOrUpdateEnterpriseProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertOrUpdateEnterpriseProperty(enterpriseEnterpriseInsertOrUpdateEnterprisePropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseInsertOrUpdateEnterpriseProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseSetEnterpriseMaximumSegmentsBodyParam = {
      enterpriseId: 6,
      value: 9
    };
    describe('#enterpriseSetEnterpriseMaximumSegments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseSetEnterpriseMaximumSegments(enterpriseEnterpriseSetEnterpriseMaximumSegmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseSetEnterpriseMaximumSegments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseUpdateEnterpriseBodyParam = {
      enterpriseId: 3,
      name: 'string',
      _update: {
        id: 8,
        created: 'string',
        name: 'string',
        contactName: 'string',
        contactPhone: 'string',
        contactMobile: 'string',
        contactEmail: 'string',
        streetAddress: 'string',
        streetAddress2: 'string',
        city: 'string',
        state: 'string',
        postalCode: 'string',
        country: 'string',
        lat: 9,
        lon: 2,
        timezone: 'string',
        locale: 'string',
        shippingSameAsLocation: 0,
        shippingContactName: 'string',
        shippingAddress: 'string',
        shippingAddress2: 'string',
        shippingCity: 'string',
        shippingState: 'string',
        shippingCountry: 'string',
        shippingPostalCode: 'string',
        modified: 'string',
        gatewayPoolId: 8,
        networkId: 7,
        returnData: false,
        user: {
          email: 'string',
          username: 'string',
          password: 'string'
        }
      }
    };
    describe('#enterpriseUpdateEnterprise - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enterpriseUpdateEnterprise(enterpriseEnterpriseUpdateEnterpriseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-velocloud_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseUpdateEnterprise', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseUpdateEnterpriseNetworkAllocationBodyParam = {
      id: 7,
      enterpriseId: 9,
      _update: {
        enterpriseId: 7,
        name: 'string',
        data: {
          spaces: [
            {
              name: 'string',
              mode: 'static',
              cidrIp: 'string',
              cidrPrefix: 6,
              maxVlans: 3,
              vlans: [
                {
                  name: 'string',
                  vlanId: 9,
                  staticReserved: 9
                }
              ]
            }
          ]
        }
      }
    };
    describe('#enterpriseUpdateEnterpriseNetworkAllocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUpdateEnterpriseNetworkAllocation(enterpriseEnterpriseUpdateEnterpriseNetworkAllocationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseUpdateEnterpriseNetworkAllocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseUpdateEnterpriseNetworkSegmentBodyParam = {
      id: 5
    };
    describe('#enterpriseUpdateEnterpriseNetworkSegment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUpdateEnterpriseNetworkSegment(enterpriseEnterpriseUpdateEnterpriseNetworkSegmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseUpdateEnterpriseNetworkSegment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseEnterpriseUpdateEnterpriseSecurityPolicyBodyParam = {
      enterpriseId: 5,
      ipsec: {
        hash: 'SHA_256',
        encryption: 'AES_256_CBC',
        diffieHellmanGroup: 'GROUP_5',
        perfectForwardSecrecy: 'GROUP_5'
      }
    };
    describe('#enterpriseUpdateEnterpriseSecurityPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseUpdateEnterpriseSecurityPolicy(enterpriseEnterpriseUpdateEnterpriseSecurityPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enterprise', 'enterpriseUpdateEnterpriseSecurityPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMaintenanceEnterpriseGetEnterpriseUsersBodyParam = {
      id: 5,
      enterpriseId: 4
    };
    describe('#enterpriseGetEnterpriseUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseGetEnterpriseUsers(userMaintenanceEnterpriseGetEnterpriseUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMaintenance', 'enterpriseGetEnterpriseUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMaintenanceEnterpriseInsertEnterpriseUserBodyParam = {
      id: 5,
      created: 'string',
      userType: 'string',
      username: 'string',
      domain: 'string',
      password: 'string',
      firstName: 'string',
      lastName: 'string',
      officePhone: 'string',
      mobilePhone: 'string',
      isNative: true,
      isActive: true,
      isLocked: true,
      disableSecondFactor: true,
      email: 'string',
      lastLogin: 'string',
      modified: 'string',
      enterpriseId: 10,
      enterpriseProxyId: 6,
      roleId: 2
    };
    describe('#enterpriseInsertEnterpriseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseInsertEnterpriseUser(userMaintenanceEnterpriseInsertEnterpriseUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMaintenance', 'enterpriseInsertEnterpriseUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userMaintenanceEnterpriseProxyGetEnterpriseProxyUsersBodyParam = {
      enterpriseProxyId: 7,
      id: 1
    };
    describe('#enterpriseProxyGetEnterpriseProxyUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyGetEnterpriseProxyUsers(userMaintenanceEnterpriseProxyGetEnterpriseProxyUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserMaintenance', 'enterpriseProxyGetEnterpriseProxyUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseProxyEnterpriseProxyGetEnterpriseProxyGatewaysBodyParam = {
      enterpriseProxyId: 10,
      with: [
        'enterpriseAssociations'
      ]
    };
    describe('#enterpriseProxyGetEnterpriseProxyGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyGetEnterpriseProxyGateways(enterpriseProxyEnterpriseProxyGetEnterpriseProxyGatewaysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnterpriseProxy', 'enterpriseProxyGetEnterpriseProxyGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseProxyEnterpriseProxyInsertEnterpriseProxyEnterpriseBodyParam = {
      id: 5,
      created: 'string',
      name: 'string',
      contactName: 'string',
      contactPhone: 'string',
      contactMobile: 'string',
      contactEmail: 'string',
      streetAddress: 'string',
      streetAddress2: 'string',
      city: 'string',
      state: 'string',
      postalCode: 'string',
      country: 'string',
      lat: 2,
      lon: 2,
      timezone: 'string',
      locale: 'string',
      shippingSameAsLocation: 1,
      shippingContactName: 'string',
      shippingAddress: 'string',
      shippingAddress2: 'string',
      shippingCity: 'string',
      shippingState: 'string',
      shippingCountry: 'string',
      shippingPostalCode: 'string',
      modified: 'string',
      gatewayPoolId: 4,
      networkId: 2,
      returnData: true,
      user: {
        email: 'string',
        password: 'string',
        password2: 'string',
        username: 'string'
      },
      configurationId: 7,
      enableEnterpriseDelegationToOperator: true,
      enableEnterpriseDelegationToProxy: true,
      enableEnterpriseUserManagementDelegationToOperator: false,
      licenses: {
        ids: [
          4
        ]
      },
      enterpriseProxyId: 3
    };
    describe('#enterpriseProxyInsertEnterpriseProxyEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyInsertEnterpriseProxyEnterprise(enterpriseProxyEnterpriseProxyInsertEnterpriseProxyEnterpriseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnterpriseProxy', 'enterpriseProxyInsertEnterpriseProxyEnterprise', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enterpriseProxyEnterpriseProxyInsertOrUpdateEnterpriseProxyPropertyBodyParam = {
      enterpriseProxyId: 10,
      name: 'string',
      value: 'string'
    };
    describe('#enterpriseProxyInsertOrUpdateEnterpriseProxyProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enterpriseProxyInsertOrUpdateEnterpriseProxyProperty(enterpriseProxyEnterpriseProxyInsertOrUpdateEnterpriseProxyPropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EnterpriseProxy', 'enterpriseProxyInsertOrUpdateEnterpriseProxyProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventEventGetEnterpriseEventsBodyParam = {
      enterpriseId: 9,
      interval: {
        end: 'string',
        start: 'string'
      },
      filter: {
        limit: 2048,
        rules: [
          {
            field: 'edgeName',
            op: 'startsWith',
            values: [
              'string'
            ]
          }
        ]
      },
      edgeId: [
        2
      ]
    };
    describe('#eventGetEnterpriseEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.eventGetEnterpriseEvents(eventEventGetEnterpriseEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'eventGetEnterpriseEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventEventGetOperatorEventsBodyParam = {
      networkId: 5,
      interval: {
        end: 'string',
        start: 'string'
      },
      filter: {
        limit: 2048,
        rules: [
          {
            field: 'event',
            op: 'endsWith',
            values: [
              'string'
            ]
          }
        ]
      },
      gatewayId: [
        6
      ]
    };
    describe('#eventGetOperatorEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.eventGetOperatorEvents(eventEventGetOperatorEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'eventGetOperatorEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventEventGetProxyEventsBodyParam = {
      interval: {
        end: 'string',
        start: 'string'
      },
      filter: {
        limit: 2048,
        rules: [
          {
            field: 'message',
            op: 'notEndsWith',
            values: [
              'string'
            ]
          }
        ]
      },
      enterpriseId: [
        8
      ]
    };
    describe('#eventGetProxyEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.eventGetProxyEvents(eventEventGetProxyEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'eventGetProxyEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallFirewallGetEnterpriseFirewallLogsBodyParam = {
      enterpriseId: 9,
      interval: {
        end: 'string',
        start: 'string'
      },
      filter: {
        limit: 4
      },
      rules: [
        'string'
      ],
      sourceIps: [
        'string'
      ],
      destIps: [
        'string'
      ],
      edges: [
        10
      ],
      with: [
        'rules'
      ]
    };
    describe('#firewallGetEnterpriseFirewallLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.firewallGetEnterpriseFirewallLogs(firewallFirewallGetEnterpriseFirewallLogsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metaData);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.rules);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'firewallGetEnterpriseFirewallLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gatewayGatewayDeleteGatewayBodyParam = {
      id: 5
    };
    describe('#gatewayDeleteGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gatewayDeleteGateway(gatewayGatewayDeleteGatewayBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateway', 'gatewayDeleteGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gatewayGatewayGatewayProvisionBodyParam = {
      networkId: 9,
      ipAddress: 'string'
    };
    describe('#gatewayGatewayProvision - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gatewayGatewayProvision(gatewayGatewayGatewayProvisionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.activationKey);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.logicalId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateway', 'gatewayGatewayProvision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gatewayGatewayGetGatewayEdgeAssignmentsBodyParam = {
      gatewayId: 5
    };
    describe('#gatewayGetGatewayEdgeAssignments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gatewayGetGatewayEdgeAssignments(gatewayGatewayGetGatewayEdgeAssignmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateway', 'gatewayGetGatewayEdgeAssignments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gatewayGatewayUpdateGatewayAttributesBodyParam = {
      id: 9
    };
    describe('#gatewayUpdateGatewayAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gatewayUpdateGatewayAttributes(gatewayGatewayUpdateGatewayAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gateway', 'gatewayUpdateGatewayAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkQualityEventLinkQualityEventGetLinkQualityEventsBodyParam = {
      edgeId: 3,
      interval: {}
    };
    describe('#linkQualityEventGetLinkQualityEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.linkQualityEventGetLinkQualityEvents(linkQualityEventLinkQualityEventGetLinkQualityEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.overallLinkQuality);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkQualityEvent', 'linkQualityEventGetLinkQualityEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNetworkDeleteNetworkGatewayPoolBodyParam = {
      id: 10
    };
    describe('#networkDeleteNetworkGatewayPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkDeleteNetworkGatewayPool(networkNetworkDeleteNetworkGatewayPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkDeleteNetworkGatewayPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNetworkGetNetworkGatewaysBodyParam = {
      networkId: 5,
      gatewayIds: [
        'string'
      ],
      with: [
        'roles'
      ]
    };
    describe('#networkGetNetworkGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkGetNetworkGateways(networkNetworkGetNetworkGatewaysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkGetNetworkGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNetworkInsertNetworkGatewayPoolBodyParam = {
      networkId: 5,
      name: 'string'
    };
    describe('#networkInsertNetworkGatewayPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkInsertNetworkGatewayPool(networkNetworkInsertNetworkGatewayPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkInsertNetworkGatewayPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNetworkUpdateNetworkGatewayPoolAttributesBodyParam = {
      id: 1
    };
    describe('#networkUpdateNetworkGatewayPoolAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkUpdateNetworkGatewayPoolAttributes(networkNetworkUpdateNetworkGatewayPoolAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkUpdateNetworkGatewayPoolAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleRoleSetEnterpriseDelegatedToEnterpriseProxyBodyParam = {
      isDelegated: true
    };
    describe('#roleSetEnterpriseDelegatedToEnterpriseProxy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.roleSetEnterpriseDelegatedToEnterpriseProxy(roleRoleSetEnterpriseDelegatedToEnterpriseProxyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.isDelegated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'roleSetEnterpriseDelegatedToEnterpriseProxy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleRoleSetEnterpriseDelegatedToOperatorBodyParam = {
      isDelegated: true
    };
    describe('#roleSetEnterpriseDelegatedToOperator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.roleSetEnterpriseDelegatedToOperator(roleRoleSetEnterpriseDelegatedToOperatorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.isDelegated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'roleSetEnterpriseDelegatedToOperator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleRoleSetEnterpriseProxyDelegatedToOperatorBodyParam = {
      isDelegated: false
    };
    describe('#roleSetEnterpriseProxyDelegatedToOperator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.roleSetEnterpriseProxyDelegatedToOperator(roleRoleSetEnterpriseProxyDelegatedToOperatorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.isDelegated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'roleSetEnterpriseProxyDelegatedToOperator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleRoleSetEnterpriseUserManagementDelegatedToOperatorBodyParam = {
      isDelegated: true
    };
    describe('#roleSetEnterpriseUserManagementDelegatedToOperator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.roleSetEnterpriseUserManagementDelegatedToOperator(roleRoleSetEnterpriseUserManagementDelegatedToOperatorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.isDelegated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'roleSetEnterpriseUserManagementDelegatedToOperator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPropertySystemPropertyGetSystemPropertiesBodyParam = {
      group: 'string',
      normalize: true
    };
    describe('#systemPropertyGetSystemProperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemPropertyGetSystemProperties(systemPropertySystemPropertyGetSystemPropertiesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemProperty', 'systemPropertyGetSystemProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPropertySystemPropertyGetSystemPropertyBodyParam = {
      name: 'string',
      id: 9
    };
    describe('#systemPropertyGetSystemProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemPropertyGetSystemProperty(systemPropertySystemPropertyGetSystemPropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.defaultValue);
                assert.equal(true, data.response.isReadOnly);
                assert.equal(false, data.response.isPassword);
                assert.equal('BOOLEAN', data.response.dataType);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemProperty', 'systemPropertyGetSystemProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPropertySystemPropertyInsertOrUpdateSystemPropertyBodyParam = {
      name: 'string',
      value: 'string'
    };
    describe('#systemPropertyInsertOrUpdateSystemProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemPropertyInsertOrUpdateSystemProperty(systemPropertySystemPropertyInsertOrUpdateSystemPropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemProperty', 'systemPropertyInsertOrUpdateSystemProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPropertySystemPropertyInsertSystemPropertyBodyParam = {
      name: 'string',
      value: 'string'
    };
    describe('#systemPropertyInsertSystemProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemPropertyInsertSystemProperty(systemPropertySystemPropertyInsertSystemPropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemProperty', 'systemPropertyInsertSystemProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPropertySystemPropertyUpdateSystemPropertyBodyParam = {
      _update: {}
    };
    describe('#systemPropertyUpdateSystemProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemPropertyUpdateSystemProperty(systemPropertySystemPropertyUpdateSystemPropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemProperty', 'systemPropertyUpdateSystemProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
