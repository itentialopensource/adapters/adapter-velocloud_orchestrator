## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for VeloCloud Orchestrator. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for VeloCloud Orchestrator.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the VMware VeloCloud Orchestrator. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">loginOperatorLogin(authorization, callback)</td>
    <td style="padding:15px">Authenticate operator user</td>
    <td style="padding:15px">{base_path}/{version}/login/operatorLogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginEnterpriseLogin(authorization, callback)</td>
    <td style="padding:15px">Authenticate enterprise or partner (MSP) user</td>
    <td style="padding:15px">{base_path}/{version}/login/enterpriseLogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogout(callback)</td>
    <td style="padding:15px">Logout and invalidate authorization session cookie</td>
    <td style="padding:15px">{base_path}/{version}/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMetaApiPath(apiPath, callback)</td>
    <td style="padding:15px">Get Swagger specification for any VCO API call</td>
    <td style="padding:15px">{base_path}/{version}/meta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationCloneEnterpriseTemplate(body, callback)</td>
    <td style="padding:15px">Clone default enterprise configuration profile</td>
    <td style="padding:15px">{base_path}/{version}/configuration/cloneEnterpriseTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationDeleteConfiguration(body, callback)</td>
    <td style="padding:15px">Delete configuration profile</td>
    <td style="padding:15px">{base_path}/{version}/configuration/deleteConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationGetConfiguration(body, callback)</td>
    <td style="padding:15px">Get configuration profile</td>
    <td style="padding:15px">{base_path}/{version}/configuration/getConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationGetIdentifiableApplications(body, callback)</td>
    <td style="padding:15px">Get the applications that are identifiable through DPI.</td>
    <td style="padding:15px">{base_path}/{version}/configuration/getIdentifiableApplications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationGetRoutableApplications(body, callback)</td>
    <td style="padding:15px">Get first packet routable applications</td>
    <td style="padding:15px">{base_path}/{version}/configuration/getRoutableApplications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryConfigureActiveForReplication(body, callback)</td>
    <td style="padding:15px">Configure current VCO for disaster recovery</td>
    <td style="padding:15px">{base_path}/{version}/disasterRecovery/configureActiveForReplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryDemoteActive(body, callback)</td>
    <td style="padding:15px">Demote current VCO from active to zombie</td>
    <td style="padding:15px">{base_path}/{version}/disasterRecovery/demoteActive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryGetReplicationBlob(body, callback)</td>
    <td style="padding:15px">Get blob needed to configure VCO replication on standby</td>
    <td style="padding:15px">{base_path}/{version}/disasterRecovery/getReplicationBlob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryGetReplicationStatus(body, callback)</td>
    <td style="padding:15px">Get disaster recovery status</td>
    <td style="padding:15px">{base_path}/{version}/disasterRecovery/getReplicationStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryPrepareForStandby(body, callback)</td>
    <td style="padding:15px">Designate current VCO as DR standby candidate</td>
    <td style="padding:15px">{base_path}/{version}/disasterRecovery/prepareForStandby?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryPromoteStandbyToActive(body, callback)</td>
    <td style="padding:15px">Promote standby VCO to active</td>
    <td style="padding:15px">{base_path}/{version}/disasterRecovery/promoteStandbyToActive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryRemoveStandby(body, callback)</td>
    <td style="padding:15px">Remove VCO disaster recovery on current server</td>
    <td style="padding:15px">{base_path}/{version}/disasterRecovery/removeStandby?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryTransitionToStandby(body, callback)</td>
    <td style="padding:15px">Transition VCO to standby</td>
    <td style="padding:15px">{base_path}/{version}/disasterRecovery/transitionToStandby?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeDeleteEdge(body, callback)</td>
    <td style="padding:15px">Delete Edge</td>
    <td style="padding:15px">{base_path}/{version}/edge/deleteEdge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeDeleteEdgeBgpNeighborRecords(body, callback)</td>
    <td style="padding:15px">Delete Edge BGP neighbor records</td>
    <td style="padding:15px">{base_path}/{version}/edge/deleteEdgeBgpNeighborRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeSetEdgeEnterpriseConfiguration(body, callback)</td>
    <td style="padding:15px">Apply configuration profile to Edge</td>
    <td style="padding:15px">{base_path}/{version}/edge/setEdgeEnterpriseConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeSetEdgeHandOffGateways(body, callback)</td>
    <td style="padding:15px">Set Edge on premise hand-off gateway(s)</td>
    <td style="padding:15px">{base_path}/{version}/edge/setEdgeHandOffGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeSetEdgeOperatorConfiguration(body, callback)</td>
    <td style="padding:15px">Apply operator profile to Edge</td>
    <td style="padding:15px">{base_path}/{version}/edge/setEdgeOperatorConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseDeleteEnterprise(body, callback)</td>
    <td style="padding:15px">Delete enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/deleteEnterprise?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseDeleteEnterpriseGatewayRecords(body, callback)</td>
    <td style="padding:15px">Delete gateway BGP neighbor record(s) for enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/deleteEnterpriseGatewayRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseDeleteEnterpriseNetworkAllocation(body, callback)</td>
    <td style="padding:15px">Delete enterprise network allocation</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/deleteEnterpriseNetworkAllocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseDeleteEnterpriseNetworkSegment(body, callback)</td>
    <td style="padding:15px">Delete an enterprise network segment</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/deleteEnterpriseNetworkSegment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseDeleteEnterpriseService(body, callback)</td>
    <td style="padding:15px">Delete enterprise network service</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/deleteEnterpriseService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseAddresses(body, callback)</td>
    <td style="padding:15px">Get enterprise IP address(es)</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseAddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseAlertConfigurations(body, callback)</td>
    <td style="padding:15px">Get enterprise alert configuration(s)</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseAlertConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseAlerts(body, callback)</td>
    <td style="padding:15px">Get triggered enterprise alerts</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseAllAlertRecipients(body, callback)</td>
    <td style="padding:15px">Get recipients receiving all enterprise alerts</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseAllAlertsRecipients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseConfigurations(body, callback)</td>
    <td style="padding:15px">Get enterprise configuration profiles</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseNetworkAllocations(body, callback)</td>
    <td style="padding:15px">Get enterprise network allocations</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseNetworkAllocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseNetworkSegments(body, callback)</td>
    <td style="padding:15px">Get all network segment objects defined on an enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseNetworkSegments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseRouteConfiguration(body, callback)</td>
    <td style="padding:15px">Get enterprise global routing preferences</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseRouteConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseRouteTable(body, callback)</td>
    <td style="padding:15px">Get enterprise route table</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseRouteTable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseServices(body, callback)</td>
    <td style="padding:15px">Get enterprise network services</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertEnterpriseService(body, callback)</td>
    <td style="padding:15px">Create enterprise service</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertEnterpriseService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertOrUpdateEnterpriseAlertConfigurations(body, callback)</td>
    <td style="padding:15px">Create, update, or delete enterprise alert configurations</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertOrUpdateEnterpriseAlertConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseSetEnterpriseAllAlertRecipients(body, callback)</td>
    <td style="padding:15px">Specify recipients for all enterprise alerts</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/setEnterpriseAllAlertsRecipients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseSetEnterpriseOperatorConfiguration(body, callback)</td>
    <td style="padding:15px">Apply operator profile to given enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/setEnterpriseOperatorConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUpdateEnterpriseRoute(body, callback)</td>
    <td style="padding:15px">Update preferred VPN exits for a route</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/updateEnterpriseRoute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUpdateEnterpriseRouteConfiguration(body, callback)</td>
    <td style="padding:15px">Update enterprise global routing preferences</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/updateEnterpriseRouteConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUpdateEnterpriseService(body, callback)</td>
    <td style="padding:15px">Update enterprise network service</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/updateEnterpriseService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyDeleteEnterpriseProxyUser(body, callback)</td>
    <td style="padding:15px">Delete partner admin user</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/deleteEnterpriseProxyUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyGetEnterpriseProxyEdgeInventory(body, callback)</td>
    <td style="padding:15px">Get partner enterprises and associated Edge inventory</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/getEnterpriseProxyEdgeInventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyGetEnterpriseProxyEnterprises(body, callback)</td>
    <td style="padding:15px">Get partner enterprises</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/getEnterpriseProxyEnterprises?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyGetEnterpriseProxyGatewayPools(body, callback)</td>
    <td style="padding:15px">Get gateway pools</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/getEnterpriseProxyGatewayPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyGetEnterpriseProxyOperatorProfiles(body, callback)</td>
    <td style="padding:15px">Get partner operator profiles</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/getEnterpriseProxyOperatorProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyGetEnterpriseProxyUser(body, callback)</td>
    <td style="padding:15px">Get enterprise proxy user</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/getEnterpriseProxyUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyInsertEnterpriseProxyUser(body, callback)</td>
    <td style="padding:15px">Create partner admin user</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/insertEnterpriseProxyUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyUpdateEnterpriseProxyUser(body, callback)</td>
    <td style="padding:15px">Update enterprise proxy admin user</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/updateEnterpriseProxyUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUserDeleteEnterpriseUser(body, callback)</td>
    <td style="padding:15px">Delete enterprise user</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseUser/deleteEnterpriseUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUserGetEnterpriseUser(body, callback)</td>
    <td style="padding:15px">Get enterprise user</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseUser/getEnterpriseUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUserUpdateEnterpriseUser(body, callback)</td>
    <td style="padding:15px">Update enterprise user</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseUser/updateEnterpriseUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeAppLinkMetrics(body, callback)</td>
    <td style="padding:15px">Get flow metric aggregate data</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeAppLinkMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeAppLinkSeries(body, callback)</td>
    <td style="padding:15px">Get flow metric time series data</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeAppLinkSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeAppMetrics(body, callback)</td>
    <td style="padding:15px">Get flow metric aggregate data by application</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeAppMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeAppSeries(body, callback)</td>
    <td style="padding:15px">Get flow metric time series data by application</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeAppSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeCategoryMetrics(body, callback)</td>
    <td style="padding:15px">Get flow metric aggregate data by application category</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeCategoryMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeCategorySeries(body, callback)</td>
    <td style="padding:15px">Get flow metric time series data by application category</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeCategorySeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeDestMetrics(body, callback)</td>
    <td style="padding:15px">Get flow metric aggregate data by destination</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeDestMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeDestSeries(body, callback)</td>
    <td style="padding:15px">Get flow metric time series data by destination</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeDestSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeDeviceMetrics(body, callback)</td>
    <td style="padding:15px">Get flow metric aggregate data by client device</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeDeviceMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeDeviceSeries(body, callback)</td>
    <td style="padding:15px">Get flow metric time series data by client device</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeDeviceSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeLinkMetrics(body, callback)</td>
    <td style="padding:15px">Get summary link metrics for an Edge</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeLinkMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeLinkSeries(body, callback)</td>
    <td style="padding:15px">Get link metric time series data for an Edge</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeLinkSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeOsMetrics(body, callback)</td>
    <td style="padding:15px">Get flow metric aggregate data by client OS</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeOsMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeOsSeries(body, callback)</td>
    <td style="padding:15px">Get flow metric time series data by client OS</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeOsSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeSegmentMetrics(body, callback)</td>
    <td style="padding:15px">Get flow metric aggregate data by segment Id</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeSegmentMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeSegmentSeries(body, callback)</td>
    <td style="padding:15px">Get flow metric time series data by segment id</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeSegmentSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeStatusMetrics(body, callback)</td>
    <td style="padding:15px">Get Edge healthStats metrics for an interval</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeStatusMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetEdgeStatusSeries(body, callback)</td>
    <td style="padding:15px">Get Edge healthStats time series for an interval</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getEdgeStatusSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetGatewayStatusMetrics(body, callback)</td>
    <td style="padding:15px">Get Gateway health metric summaries for an interval</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getGatewayStatusMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsGetGatewayStatusSeries(body, callback)</td>
    <td style="padding:15px">Get Gateway health metric time series for an interval</td>
    <td style="padding:15px">{base_path}/{version}/metrics/getGatewayStatusSeries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitoringGetAggregateEdgeLinkMetrics(body, callback)</td>
    <td style="padding:15px">Get aggregate Edge link metrics across enterprises</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/getAggregateEdgeLinkMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitoringGetAggregateEnterpriseEvents(body, callback)</td>
    <td style="padding:15px">Get events across all enterprises</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/getAggregateEnterpriseEvents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitoringGetAggregates(body, callback)</td>
    <td style="padding:15px">Get aggregate enterprise and Edge information</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/getAggregates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitoringGetEnterpriseBgpPeerStatus(body, callback)</td>
    <td style="padding:15px">Get gateway BGP peer status for all enterprise gateways</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/getEnterpriseBgpPeerStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitoringGetEnterpriseEdgeBgpPeerStatus(body, callback)</td>
    <td style="padding:15px">Get Edge BGP peer status for all enterprise Edges</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/getEnterpriseEdgeBgpPeerStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitoringGetEnterpriseEdgeClusterStatus(body, callback)</td>
    <td style="padding:15px">Get Edge Cluster status</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/getEnterpriseEdgeClusterStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitoringGetEnterpriseEdgeLinkStatus(body, callback)</td>
    <td style="padding:15px">Get Edge and link status data</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/getEnterpriseEdgeLinkStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkGetNetworkConfigurations(body, callback)</td>
    <td style="padding:15px">Get operator configuration profiles</td>
    <td style="padding:15px">{base_path}/{version}/network/getNetworkConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkGetNetworkEnterprises(body, callback)</td>
    <td style="padding:15px">Get enterprises on network</td>
    <td style="padding:15px">{base_path}/{version}/network/getNetworkEnterprises?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkGetNetworkGatewayPools(body, callback)</td>
    <td style="padding:15px">Get gateway pools on network</td>
    <td style="padding:15px">{base_path}/{version}/network/getNetworkGatewayPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkGetNetworkOperatorUsers(body, callback)</td>
    <td style="padding:15px">Get operator users for network</td>
    <td style="padding:15px">{base_path}/{version}/network/getNetworkOperatorUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">operatorUserDeleteOperatorUser(body, callback)</td>
    <td style="padding:15px">Delete operator user</td>
    <td style="padding:15px">{base_path}/{version}/operatorUser/deleteOperatorUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">operatorUserGetOperatorUser(body, callback)</td>
    <td style="padding:15px">Get operator user</td>
    <td style="padding:15px">{base_path}/{version}/operatorUser/getOperatorUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">operatorUserInsertOperatorUser(body, callback)</td>
    <td style="padding:15px">Create operator user</td>
    <td style="padding:15px">{base_path}/{version}/operatorUser/insertOperatorUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">operatorUserUpdateOperatorUser(body, callback)</td>
    <td style="padding:15px">Update operator user</td>
    <td style="padding:15px">{base_path}/{version}/operatorUser/updateOperatorUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleCreateRoleCustomization(body, callback)</td>
    <td style="padding:15px">Create role customization</td>
    <td style="padding:15px">{base_path}/{version}/role/createRoleCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleDeleteRoleCustomization(body, callback)</td>
    <td style="padding:15px">Delete role customization</td>
    <td style="padding:15px">{base_path}/{version}/role/deleteRoleCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleGetUserTypeRoles(body, callback)</td>
    <td style="padding:15px">Get roles per user type</td>
    <td style="padding:15px">{base_path}/{version}/role/getUserTypeRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vcoDiagnosticsGetVcoDbDiagnostics(body, callback)</td>
    <td style="padding:15px">Get VCO Database Diagnostics</td>
    <td style="padding:15px">{base_path}/{version}/vcoDiagnostics/getVcoDbDiagnostics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vcoInventoryAssociateEdge(body, callback)</td>
    <td style="padding:15px">Assign Edge to enterprise</td>
    <td style="padding:15px">{base_path}/{version}/vcoInventory/associateEdge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vcoInventoryGetInventoryItems(body, callback)</td>
    <td style="padding:15px">Get available VCO inventory items</td>
    <td style="padding:15px">{base_path}/{version}/vcoInventory/getInventoryItems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vpnGenerateVpnGatewayConfiguration(body, callback)</td>
    <td style="padding:15px">Provision a non-SD-WAN VPN site</td>
    <td style="padding:15px">{base_path}/{version}/vpn/generateVpnGatewayConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setClientDeviceHostName(body, callback)</td>
    <td style="padding:15px">Set hostname for client device</td>
    <td style="padding:15px">{base_path}/{version}/clientDevice/setClientDeviceHostName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationCloneAndConvertConfiguration(body, callback)</td>
    <td style="padding:15px">Create new segment-based profile from existing network-based profile</td>
    <td style="padding:15px">{base_path}/{version}/configuration/cloneAndConvertConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationCloneConfiguration(body, callback)</td>
    <td style="padding:15px">Clone configuration profile</td>
    <td style="padding:15px">{base_path}/{version}/configuration/cloneConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationGetConfigurationModules(body, callback)</td>
    <td style="padding:15px">Get configuration profile modules</td>
    <td style="padding:15px">{base_path}/{version}/configuration/getConfigurationModules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationInsertConfigurationModule(body, callback)</td>
    <td style="padding:15px">Create configuration module</td>
    <td style="padding:15px">{base_path}/{version}/configuration/insertConfigurationModule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationUpdateConfiguration(body, callback)</td>
    <td style="padding:15px">Update configuration profile</td>
    <td style="padding:15px">{base_path}/{version}/configuration/updateConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationUpdateConfigurationModule(body, callback)</td>
    <td style="padding:15px">Update configuration module</td>
    <td style="padding:15px">{base_path}/{version}/configuration/updateConfigurationModule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeEdgeCancelReactivation(body, callback)</td>
    <td style="padding:15px">Cancel pending Edge reactivation request</td>
    <td style="padding:15px">{base_path}/{version}/edge/edgeCancelReactivation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeEdgeProvision(body, callback)</td>
    <td style="padding:15px">Provision Edge</td>
    <td style="padding:15px">{base_path}/{version}/edge/edgeProvision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeEdgeRequestReactivation(body, callback)</td>
    <td style="padding:15px">Prepare Edge for reactivation</td>
    <td style="padding:15px">{base_path}/{version}/edge/edgeRequestReactivation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeGetClientVisibilityMode(body, callback)</td>
    <td style="padding:15px">Get an edge's client visibility mode</td>
    <td style="padding:15px">{base_path}/{version}/edge/getClientVisibilityMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeGetEdge(body, callback)</td>
    <td style="padding:15px">Get Edge</td>
    <td style="padding:15px">{base_path}/{version}/edge/getEdge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeGetEdgeActivationEmail(body, callback)</td>
    <td style="padding:15px">Get Edge Activation E-mail</td>
    <td style="padding:15px">{base_path}/{version}/edge/getEdgeActivationEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeGetEdgeConfigurationModules(body, callback)</td>
    <td style="padding:15px">Get edge configuration modules</td>
    <td style="padding:15px">{base_path}/{version}/edge/getEdgeConfigurationModules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeGetEdgeConfigurationStack(body, callback)</td>
    <td style="padding:15px">Get Edge configuration stack</td>
    <td style="padding:15px">{base_path}/{version}/edge/getEdgeConfigurationStack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeSendEdgeActivationEmail(body, callback)</td>
    <td style="padding:15px">Get Edge</td>
    <td style="padding:15px">{base_path}/{version}/edge/sendEdgeActivationEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeUpdateEdgeAdminPassword(body, callback)</td>
    <td style="padding:15px">Update Edge local UI authentication credentials</td>
    <td style="padding:15px">{base_path}/{version}/edge/updateEdgeAdminPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeUpdateEdgeAttributes(body, callback)</td>
    <td style="padding:15px">Update Edge attributes</td>
    <td style="padding:15px">{base_path}/{version}/edge/updateEdgeAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeUpdateEdgeCredentialsByConfiguration(body, callback)</td>
    <td style="padding:15px">Update Edge local UI credentials by configuration profile</td>
    <td style="padding:15px">{base_path}/{version}/edge/updateEdgeCredentialsByConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseDecodeEnterpriseKey(body, callback)</td>
    <td style="padding:15px">Decrypt an enterprise key</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/decodeEnterpriseKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseEncodeEnterpriseKey(body, callback)</td>
    <td style="padding:15px">Encrypt an enterprise key</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/encodeEnterpriseKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterprise(body, callback)</td>
    <td style="padding:15px">Get enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterprise?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseCapabilities(body, callback)</td>
    <td style="padding:15px">Get enterprise capabilities</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseCapabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseEdges(body, callback)</td>
    <td style="padding:15px">Get enterprise Edges</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseEdges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseGatewayHandoff(body, callback)</td>
    <td style="padding:15px">Get enterprise gateway handoff configuration</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseGatewayHandoff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseMaximumSegments(body, callback)</td>
    <td style="padding:15px">Get enterprise maximum segment</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseMaximumSegments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseNetworkAllocation(body, callback)</td>
    <td style="padding:15px">Get enterprise network allocation</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseNetworkAllocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseProperty(body, callback)</td>
    <td style="padding:15px">Get enterprise property</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertEnterprise(body, callback)</td>
    <td style="padding:15px">Create enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertEnterprise?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertEnterpriseNetworkAllocation(body, callback)</td>
    <td style="padding:15px">Create enterprise network allocation</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertEnterpriseNetworkAllocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertEnterpriseNetworkSegment(body, callback)</td>
    <td style="padding:15px">Create enterprise network segment</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertEnterpriseNetworkSegment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertOrUpdateEnterpriseCapability(body, callback)</td>
    <td style="padding:15px">Create or update enterprise capability</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertOrUpdateEnterpriseCapability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertOrUpdateEnterpriseGatewayHandoff(body, callback)</td>
    <td style="padding:15px">Create or update enterprise gateway handoff configuration</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertOrUpdateEnterpriseGatewayHandoff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertOrUpdateEnterpriseProperty(body, callback)</td>
    <td style="padding:15px">Insert or update an enterprise property</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertOrUpdateEnterpriseProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseSetEnterpriseMaximumSegments(body, callback)</td>
    <td style="padding:15px">Set enterprise maximum segments</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/setEnterpriseMaximumSegments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUpdateEnterprise(body, callback)</td>
    <td style="padding:15px">Update enterprise</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/updateEnterprise?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUpdateEnterpriseNetworkAllocation(body, callback)</td>
    <td style="padding:15px">Update enterprise network allocation</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/updateEnterpriseNetworkAllocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUpdateEnterpriseNetworkSegment(body, callback)</td>
    <td style="padding:15px">Update an enterprise network segment</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/updateEnterpriseNetworkSegment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseUpdateEnterpriseSecurityPolicy(body, callback)</td>
    <td style="padding:15px">Update enterprise security policy</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/updateEnterpriseSecurityPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyGetEnterpriseProxyProperty(body, callback)</td>
    <td style="padding:15px">Get enterprise proxy property</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseProxyProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseGetEnterpriseUsers(body, callback)</td>
    <td style="padding:15px">Get enterprise users</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/getEnterpriseUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseInsertEnterpriseUser(body, callback)</td>
    <td style="padding:15px">Create enterprise user</td>
    <td style="padding:15px">{base_path}/{version}/enterprise/insertEnterpriseUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyGetEnterpriseProxyUsers(body, callback)</td>
    <td style="padding:15px">Get enterprise proxy admin users</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/getEnterpriseProxyUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyGetEnterpriseProxyGateways(body, callback)</td>
    <td style="padding:15px">Get gateways for enterprise proxy</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/getEnterpriseProxyGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyInsertEnterpriseProxyEnterprise(body, callback)</td>
    <td style="padding:15px">Create enterprise owned by MSP</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/insertEnterpriseProxyEnterprise?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseProxyInsertOrUpdateEnterpriseProxyProperty(body, callback)</td>
    <td style="padding:15px">Insert or update an enterprise proxy (MSP) property</td>
    <td style="padding:15px">{base_path}/{version}/enterpriseProxy/insertOrUpdateEnterpriseProxyProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventGetEnterpriseEvents(body, callback)</td>
    <td style="padding:15px">Get Edge events</td>
    <td style="padding:15px">{base_path}/{version}/event/getEnterpriseEvents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventGetOperatorEvents(body, callback)</td>
    <td style="padding:15px">Get operator events</td>
    <td style="padding:15px">{base_path}/{version}/event/getOperatorEvents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventGetProxyEvents(body, callback)</td>
    <td style="padding:15px">Fetch Partner events</td>
    <td style="padding:15px">{base_path}/{version}/event/getProxyEvents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewallGetEnterpriseFirewallLogs(body, callback)</td>
    <td style="padding:15px">Get enterprise firewall logs</td>
    <td style="padding:15px">{base_path}/{version}/firewall/getEnterpriseFirewallLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gatewayDeleteGateway(body, callback)</td>
    <td style="padding:15px">Delete a gateway</td>
    <td style="padding:15px">{base_path}/{version}/gateway/deleteGateway?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gatewayGatewayProvision(body, callback)</td>
    <td style="padding:15px">Provision gateway</td>
    <td style="padding:15px">{base_path}/{version}/gateway/gatewayProvision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gatewayGetGatewayEdgeAssignments(body, callback)</td>
    <td style="padding:15px">Get edge assignments for a gateway</td>
    <td style="padding:15px">{base_path}/{version}/gateway/getGatewayEdgeAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gatewayUpdateGatewayAttributes(body, callback)</td>
    <td style="padding:15px">Update gateway attributes</td>
    <td style="padding:15px">{base_path}/{version}/gateway/updateGatewayAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">linkQualityEventGetLinkQualityEvents(body, callback)</td>
    <td style="padding:15px">Get link quality (QoE) scores for an Edge</td>
    <td style="padding:15px">{base_path}/{version}/linkQualityEvent/getLinkQualityEvents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkDeleteNetworkGatewayPool(body, callback)</td>
    <td style="padding:15px">Delete gateway pool</td>
    <td style="padding:15px">{base_path}/{version}/network/deleteNetworkGatewayPool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkGetNetworkGateways(body, callback)</td>
    <td style="padding:15px">Get gateways on network</td>
    <td style="padding:15px">{base_path}/{version}/network/getNetworkGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkInsertNetworkGatewayPool(body, callback)</td>
    <td style="padding:15px">Create gateway pool</td>
    <td style="padding:15px">{base_path}/{version}/network/insertNetworkGatewayPool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkUpdateNetworkGatewayPoolAttributes(body, callback)</td>
    <td style="padding:15px">Update gateway pool attributes</td>
    <td style="padding:15px">{base_path}/{version}/network/updateNetworkGatwayPoolAttributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleSetEnterpriseDelegatedToEnterpriseProxy(body, callback)</td>
    <td style="padding:15px">Grant enterprise access to partner</td>
    <td style="padding:15px">{base_path}/{version}/role/setEnterpriseDelegatedToEnterpriseProxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleSetEnterpriseDelegatedToOperator(body, callback)</td>
    <td style="padding:15px">Grant enterprise access to network operator</td>
    <td style="padding:15px">{base_path}/{version}/role/setEnterpriseDelegatedToOperator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleSetEnterpriseProxyDelegatedToOperator(body, callback)</td>
    <td style="padding:15px">Grant enterprise proxy access to network operator</td>
    <td style="padding:15px">{base_path}/{version}/role/setEnterpriseProxyDelegatedToOperator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleSetEnterpriseUserManagementDelegatedToOperator(body, callback)</td>
    <td style="padding:15px">Grant enterprise user access to network operator</td>
    <td style="padding:15px">{base_path}/{version}/role/setEnterpriseUserManagementDelegatedToOperator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemPropertyGetSystemProperties(body, callback)</td>
    <td style="padding:15px">Get all system properties</td>
    <td style="padding:15px">{base_path}/{version}/systemProperty/getSystemProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemPropertyGetSystemProperty(body, callback)</td>
    <td style="padding:15px">Get system property</td>
    <td style="padding:15px">{base_path}/{version}/systemProperty/getSystemProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemPropertyInsertOrUpdateSystemProperty(body, callback)</td>
    <td style="padding:15px">Create or update system property</td>
    <td style="padding:15px">{base_path}/{version}/systemProperty/insertOrUpdateSystemProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemPropertyInsertSystemProperty(body, callback)</td>
    <td style="padding:15px">Create system property</td>
    <td style="padding:15px">{base_path}/{version}/systemProperty/insertSystemProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemPropertyUpdateSystemProperty(body, callback)</td>
    <td style="padding:15px">Update system property</td>
    <td style="padding:15px">{base_path}/{version}/systemProperty/updateSystemProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
