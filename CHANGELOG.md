
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:44PM

See merge request itentialopensource/adapters/adapter-velocloud_orchestrator!15

---

## 0.4.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-velocloud_orchestrator!13

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:02PM

See merge request itentialopensource/adapters/adapter-velocloud_orchestrator!12

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:14PM

See merge request itentialopensource/adapters/adapter-velocloud_orchestrator!11

---

## 0.4.0 [05-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud_orchestrator!10

---

## 0.3.3 [03-26-2024]

* Changes made at 2024.03.26_14:09PM

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud_orchestrator!9

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_10:42AM

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud_orchestrator!8

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:05PM

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud_orchestrator!7

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud_orchestrator!5

---

## 0.2.1 [09-11-2023]

* Revert "More migration changes"

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud_orchestrator!6

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud_orchestrator!2

---

## 0.1.1 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud_orchestrator!1

---
